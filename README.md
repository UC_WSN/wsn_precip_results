# WSN_Precip

Figures of precipitation estimates using wireless-sensor network for identified precipitation events in WY 2014-2017.

For the paper of Cui et al. (2021).
